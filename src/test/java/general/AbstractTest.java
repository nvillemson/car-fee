package general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swedbank.Application;
import com.swedbank.persistence.enums.RiskType;
import com.swedbank.web.request.AddRiskRequest;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.URL;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class AbstractTest {

    @LocalServerPort
    private int port;

    protected ObjectMapper mapper;

    protected String addRiskUrl;
    protected String addCarUrl;
    protected String getCarsUrl;
    protected String getRisksUrl;
    protected String updateCarUrl;
    protected String updateRisksUrl;
    protected String deleteRisksUrl;
    protected String deleteCarUrl;

    @Autowired
    protected TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        URL base = new URL("http://localhost:" + port + "/");
        addRiskUrl = base.toString() + "risks/add-risk";
        getRisksUrl = base.toString() + "risks/get-all-risks";
        updateRisksUrl = base.toString() + "risks/update-risk";
        deleteRisksUrl = base.toString() + "risks/delete-risk";
        addCarUrl = base.toString() + "car/add-car";
        getCarsUrl = base.toString() + "car/get-cars";
        updateCarUrl = base.toString() + "car/update-car";
        deleteCarUrl = base.toString() + "car/delete-car";
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        initializeRisks();
        //mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
    }

    protected HttpEntity<String> produceEntity(Object riskRequest) throws JsonProcessingException {
        String json = mapper.writeValueAsString(riskRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }

    protected void initializeRisks() throws JsonProcessingException {
        AddRiskRequest req = new AddRiskRequest(RiskType.AVG_PURCHASE_PRICE, "WOLKSWAGEN", BigDecimal.valueOf(1550));
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.AVG_PURCHASE_PRICE, "AUDI", BigDecimal.valueOf(15500));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.AVG_PURCHASE_PRICE, "BMW", BigDecimal.valueOf(8000));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.MAKE_COEFFICIENTS, "AUDI", BigDecimal.valueOf(0.9));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.MAKE_COEFFICIENTS, "WOLKSWAGEN", BigDecimal.valueOf(0.8));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.COEFFICIENTS, "vehicle_age", BigDecimal.valueOf(0.5));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.COEFFICIENTS, "vehicle_value", BigDecimal.valueOf(0.5));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new AddRiskRequest(RiskType.COEFFICIENTS, "previous_indemnity", BigDecimal.valueOf(0.1));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

}
