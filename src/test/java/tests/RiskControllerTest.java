package tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.swedbank.persistence.enums.RiskType;
import com.swedbank.web.request.AddRiskRequest;
import com.swedbank.web.request.IdRequest;
import com.swedbank.web.request.UpdateRiskRequest;
import com.swedbank.web.response.IdResponse;
import com.swedbank.web.response.RiskResponse;
import com.swedbank.web.response.RiskResponseByType;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.List;

import static com.swedbank.persistence.enums.RiskType.AVG_PURCHASE_PRICE;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class RiskControllerTest extends AbstractTest {

    @Test
    public void addRiskTestBadRequest() throws JsonProcessingException {
        // empty invalid request
        AddRiskRequest req = new AddRiskRequest(null, null, null);
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // invalid risk type
        req = new AddRiskRequest(null, "Test", BigDecimal.valueOf(1.5));
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        //invalid risk value
        req = new AddRiskRequest(AVG_PURCHASE_PRICE, "Test", BigDecimal.ZERO);
        entity = produceEntity(req);
        response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addRiskTestSuccess() throws JsonProcessingException {
        AddRiskRequest req = new AddRiskRequest(AVG_PURCHASE_PRICE, "Test", BigDecimal.valueOf(1.55));
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse riskId = mapper.readValue(response.getBody(), IdResponse.class);

        // select
        response = template.getForEntity(getRisksUrl, String.class);
        TypeReference<List<RiskResponse>> typeReference = new TypeReference<>() {};
        List<RiskResponse> resp = mapper.readValue(response.getBody(), typeReference);

        RiskResponse riskResponse = resp.stream().filter(risk -> AVG_PURCHASE_PRICE.equals(risk.getType()))
                .findAny()
                .orElse(null);

        assertNotNull(riskResponse);

        RiskResponseByType riskActual = riskResponse.getData().stream()
                .filter(risk -> "Test".equals(risk.getName()))
                .findAny()
                .orElse(null);

        assertNotNull(riskActual);
        assertEquals(BigDecimal.valueOf(1.55), riskActual.getValue());

        // update
        UpdateRiskRequest req2 = new UpdateRiskRequest(riskId.getId(), "Test3", BigDecimal.valueOf(1.75));
        entity = produceEntity(req2);
        response = template.postForEntity(updateRisksUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(getRisksUrl, String.class);
        resp = mapper.readValue(response.getBody(), typeReference);

        riskResponse = resp.stream().filter(risk -> AVG_PURCHASE_PRICE.equals(risk.getType()))
                .findAny()
                .orElse(null);

        assertNotNull(riskResponse);
        assertEquals(AVG_PURCHASE_PRICE, riskResponse.getType());

        riskActual = riskResponse.getData().stream()
                .filter(risk -> riskId.getId().equals(risk.getId()))
                .findAny()
                .orElse(null);

        assertNotNull(riskActual);
        assertEquals(BigDecimal.valueOf(1.75), riskActual.getValue());
        assertEquals("Test3", riskActual.getName());
    }

    @Test
    public void updateDeleteRiskTestSuccess() throws JsonProcessingException {
        AddRiskRequest req = new AddRiskRequest(AVG_PURCHASE_PRICE, "Test4", BigDecimal.valueOf(1.85));
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addRiskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse riskId = mapper.readValue(response.getBody(), IdResponse.class);

        response = template.getForEntity(getRisksUrl, String.class);
        TypeReference<List<RiskResponse>> typeReference = new TypeReference<>() {};
        List<RiskResponse> resp = mapper.readValue(response.getBody(), typeReference);

        RiskResponse riskResponse = resp.stream().filter(risk -> AVG_PURCHASE_PRICE.equals(risk.getType()))
                .findAny()
                .orElse(null);

        assertNotNull(riskResponse);

        RiskResponseByType riskActual = riskResponse.getData().stream()
                .filter(risk -> riskId.getId().equals(risk.getId()))
                .findAny()
                .orElse(null);

        assertNotNull(riskActual);
        assertEquals(BigDecimal.valueOf(1.85), riskActual.getValue());
        assertEquals("Test4", riskActual.getName());

        // delete
        IdRequest req3 = new IdRequest(riskId.getId());
        entity = produceEntity(req3);
        response = template.postForEntity(deleteRisksUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(getRisksUrl, String.class);
        resp = mapper.readValue(response.getBody(), typeReference);

        riskResponse = resp.stream().filter(risk -> AVG_PURCHASE_PRICE.equals(risk.getType()))
                .findAny()
                .orElse(null);

        assertNotNull(riskResponse);

        assertEquals(AVG_PURCHASE_PRICE, riskResponse.getType());

        riskActual = riskResponse.getData().stream()
                .filter(risk -> riskId.getId().equals(risk.getId()))
                .findAny()
                .orElse(null);

        assertNull(riskActual);
    }
}
