package tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.swedbank.web.request.AddCarRequest;
import com.swedbank.web.request.IdRequest;
import com.swedbank.web.request.UpdateCarRequest;
import com.swedbank.web.response.CarResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

public class CarControllerTest extends AbstractTest {

    @Test
    public void addCarTestBadRequest() throws JsonProcessingException {
        // empty invalid request
        AddCarRequest req = new AddCarRequest(null, null, null,
                null, null, null, null);
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // invalid request wrong price
        req = new AddCarRequest("ACD543", LocalDate.now(), 100L,
                "MAZDA", 1000L, BigDecimal.ZERO, false);
        entity = produceEntity(req);
        response = template.postForEntity(addCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // invalid request wrong producer
        req = new AddCarRequest("ACD543", LocalDate.now(), 300L,
                "MAZDA", 1000L, BigDecimal.ZERO, false);
        entity = produceEntity(req);
        response = template.postForEntity(addCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addCarTestSuccess() throws JsonProcessingException {
        AddCarRequest req = new AddCarRequest("ACD543", LocalDate.now(), 500L,
                "BMW", 1000L, BigDecimal.valueOf(100.00), false);
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));


        // select
        response = template.getForEntity(getCarsUrl + "?offset=0&limit=10", String.class);
        TypeReference<List<CarResponse>> typeReference = new TypeReference<>() {};
        List<CarResponse> resp = mapper.readValue(response.getBody(), typeReference);

        CarResponse car1 = resp.stream()
                .filter(car -> "ACD543".equals(car.getPlateNumber()))
                .findAny()
                .orElse(null);

        assertNotNull(car1);
        assertEquals("250", car1.getCascoAnnual().toBigIntegerExact().toString());

        // update car recalculate idemnity
        UpdateCarRequest req2 = new UpdateCarRequest(car1.getId(), "ACD543", LocalDate.now(), 500L,
                "BMW", 1000L, BigDecimal.valueOf(1000.00), true);
        entity = produceEntity(req2);
        response = template.postForEntity(updateCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        // select
        response = template.getForEntity(getCarsUrl + "?offset=0&limit=10", String.class);
        typeReference = new TypeReference<>() {};
        resp = mapper.readValue(response.getBody(), typeReference);

        car1 = resp.stream()
                .filter(car -> "ACD543".equals(car.getPlateNumber()))
                .findAny()
                .orElse(null);

        assertNotNull(car1);
        assertEquals("350", car1.getCascoAnnual().toBigIntegerExact().toString());
    }

    @Test
    public void deleteCarTestSuccess() throws JsonProcessingException {
        AddCarRequest req = new AddCarRequest("ACD123", LocalDate.now(), 500L,
                "BMW", 1000L, BigDecimal.valueOf(100.00), false);
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        // select
        response = template.getForEntity(getCarsUrl + "?offset=0&limit=10", String.class);
        TypeReference<List<CarResponse>> typeReference = new TypeReference<>() {};
        List<CarResponse> resp = mapper.readValue(response.getBody(), typeReference);

        CarResponse car1 = resp.stream()
                .filter(car -> "ACD123".equals(car.getPlateNumber()))
                .findAny()
                .orElse(null);

        assertNotNull(car1);

        // delete
        IdRequest req3 = new IdRequest(car1.getId());
        entity = produceEntity(req3);
        response = template.postForEntity(deleteCarUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        // select
        response = template.getForEntity(getCarsUrl + "?offset=0&limit=10", String.class);
        typeReference = new TypeReference<>() {};
        resp = mapper.readValue(response.getBody(), typeReference);

        car1 = resp.stream()
                .filter(car -> "ACD123".equals(car.getPlateNumber()))
                .findAny()
                .orElse(null);

        assertNull(car1);
    }
}
