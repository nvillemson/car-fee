package com.swedbank.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CAR")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "PLATE_NUMBER", nullable = false, unique = true, columnDefinition = "VARCHAR(6)")
    private String plateNumber;

    @Column(name = "REGISTRATION", nullable = false, columnDefinition = "DATE")
    private LocalDate registration;

    @Column(name = "PURCHASE_PRICE", nullable = false, columnDefinition = "BIGINT")
    private Long purchasePrice;

    @Column(name = "PRODUCER", nullable = false, columnDefinition = "VARCHAR(25)")
    private String producer;

    @Column(name = "MILEAGE", nullable = false)
    private Long mileage;

    @Column(name = "PREVIOUS_INDEMNITY", nullable = false, columnDefinition = "DECIMAL(8,2)")
    private BigDecimal previousIndemnity;

    @Column(name = "CASCO_ANNUAL", nullable = false, columnDefinition = "DECIMAL(8,2)")
    private BigDecimal cascoAnnual;

    @Column(name = "PREVIOUS_INDEMNITY_USED", columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean previousIndemnityUsed;

    @Column(name = "DELETED", columnDefinition = "TIMESTAMP")
    private LocalDateTime deleted;

}
