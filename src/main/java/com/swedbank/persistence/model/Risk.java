package com.swedbank.persistence.model;

import com.swedbank.persistence.enums.RiskType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RISK")
public class Risk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "RISK_TYPE", nullable = false, columnDefinition = "VARCHAR(20)")
    private RiskType type;

    @Column(name = "RISK_NAME", nullable = false, columnDefinition = "VARCHAR(30)")
    private String name;

    @Column(name = "RISK_VALUE", nullable = false, columnDefinition = "DECIMAL(8,2)")
    private BigDecimal value;

    @Column(name = "DELETED", columnDefinition = "TIMESTAMP")
    private LocalDateTime deleted;
}
