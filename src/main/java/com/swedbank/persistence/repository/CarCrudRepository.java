package com.swedbank.persistence.repository;

import com.swedbank.persistence.model.Car;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

public interface CarCrudRepository extends CrudRepository<Car, Long> {

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Car c SET c.deleted = CURRENT_TIMESTAMP() WHERE c.id = ?1")
    void removeCar(long id);

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Car c SET c.registration = :registration, c.purchasePrice = :purchasePrice, c.producer = :producer, c.mileage = :mileage, " +
            "c.previousIndemnity = :previousIndemnity, c.cascoAnnual = :cascoAnnual, c.previousIndemnityUsed = :previousIndemnityUsed WHERE c.id = :id")
    void updateCar(@Param("registration") LocalDate registration, @Param("purchasePrice") Long purchasePrice, @Param("producer") String producer,
                     @Param("mileage") Long mileage, @Param("previousIndemnity") BigDecimal previousIndemnity, @Param("cascoAnnual") BigDecimal cascoAnnual,
                     @Param("previousIndemnityUsed") Boolean previousIndemnityUsed, @Param("id") Long id);


    @Query(value="select * from Car c where c.deleted is null ORDER BY c.id LIMIT :offset, :limit", nativeQuery = true)
    Iterable<Car> listCars(@Param("offset") int offset, @Param("limit") int limit);

}
