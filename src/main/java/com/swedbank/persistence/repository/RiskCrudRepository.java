package com.swedbank.persistence.repository;

import com.swedbank.persistence.model.Risk;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.math.BigDecimal;

public interface RiskCrudRepository extends CrudRepository<Risk, Long> {

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Risk r SET r.deleted = CURRENT_TIMESTAMP() WHERE r.id = ?1")
    void removeRisk(long riskId);

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Risk r SET r.name = :riskName, r.value = :rValue WHERE r.id = :id")
    void updateRisk(@Param("id") Long id, @Param("riskName") String riskName, @Param("rValue") BigDecimal value);

    @Query("select r from  Risk r where r.deleted is null")
    Iterable<Risk> getNonDeletedRisks();
}
