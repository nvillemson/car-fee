package com.swedbank.persistence.enums;

public enum RiskType {

    COEFFICIENTS,
    MAKE_COEFFICIENTS,
    AVG_PURCHASE_PRICE

}
