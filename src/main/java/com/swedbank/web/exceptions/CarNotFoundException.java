package com.swedbank.web.exceptions;

public class CarNotFoundException extends RuntimeException {

    private String message;

    public CarNotFoundException(String message) {
        super(message);
    }
}
