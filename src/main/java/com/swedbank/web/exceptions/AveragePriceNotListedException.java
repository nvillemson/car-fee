package com.swedbank.web.exceptions;

public class AveragePriceNotListedException extends RuntimeException {

    private String message;

    public AveragePriceNotListedException(String message) {
        super(message);
    }
}
