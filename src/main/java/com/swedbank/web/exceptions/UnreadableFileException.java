package com.swedbank.web.exceptions;

public class UnreadableFileException extends RuntimeException {

    private String message;

    public UnreadableFileException(String message) {
        super(message);
    }
}
