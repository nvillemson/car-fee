package com.swedbank.web.controller;

import com.swedbank.web.request.AddCarRequest;
import com.swedbank.web.request.IdRequest;
import com.swedbank.web.request.UpdateCarRequest;
import com.swedbank.web.response.CarResponse;
import com.swedbank.web.response.IdResponse;
import com.swedbank.web.service.CarService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@SuppressWarnings("unused")
@Controller
@AllArgsConstructor
@RequestMapping("/car")
public class CarController {

    private final CarService carService;

    @PostMapping("/add-car")
    public @ResponseBody IdResponse addCar(@RequestBody @Valid AddCarRequest req) {
        return carService.addCar(req.getPlateNumber(), req.getRegistration(), req.getPurchasePrice(), req.getProducer(), req.getMileage(),
                req.getPreviousIndemnity(), req.getPreviousIndemnityUsed());
    }

    @PostMapping("/upload-cars-csv")
    @ResponseStatus(HttpStatus.OK)
    public void singleFileUpload(@RequestParam("file") MultipartFile file) {
        carService.parseAndSave(file);
    }

    @PostMapping("/update-car")
    @ResponseStatus(HttpStatus.OK)
    public void updateCar(@RequestBody @Valid UpdateCarRequest req) {
        carService.updateCar(req.getId(), req.getPlateNumber(), req.getRegistration(), req.getPurchasePrice(), req.getProducer(),
                req.getMileage(), req.getPreviousIndemnity(), req.getPreviousIndemnityUsed());
    }

    @PostMapping("/delete-car")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCar(@RequestBody @Valid IdRequest req) {
        carService.deleteCar(req.getId());
    }

    @GetMapping("/get-cars")
    public @ResponseBody List<CarResponse> getCars(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
        return carService.getCars(offset, limit);
    }

}
