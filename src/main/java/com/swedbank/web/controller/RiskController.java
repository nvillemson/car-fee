package com.swedbank.web.controller;

import com.swedbank.web.request.AddRiskRequest;
import com.swedbank.web.request.IdRequest;
import com.swedbank.web.request.UpdateRiskRequest;
import com.swedbank.web.response.IdResponse;
import com.swedbank.web.response.RiskResponse;
import com.swedbank.web.service.RiskService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@SuppressWarnings("unused")
@Controller
@AllArgsConstructor
@RequestMapping("/risks")
public class RiskController {

    private final RiskService riskService;

    @PostMapping("/add-risk")
    public @ResponseBody IdResponse addRisk(@RequestBody @Valid AddRiskRequest req) {
        return riskService.addRisk(req.getName(), req.getType(), req.getValue());
    }

    @GetMapping("/get-all-risks")
    public @ResponseBody List<RiskResponse> getAllRisks() {
        return riskService.getAllRisks();
    }

    @PostMapping("/update-risk")
    public @ResponseBody IdResponse updateRisk(@RequestBody @Valid UpdateRiskRequest req) {
        return riskService.updateRisk(req.getId(), req.getName(), req.getValue());
    }

    @PostMapping("/delete-risk")
    @ResponseStatus(HttpStatus.OK)
    public void deleteRisk(@RequestBody @Valid IdRequest req) {
        riskService.deleteRisk(req.getId());
    }
}
