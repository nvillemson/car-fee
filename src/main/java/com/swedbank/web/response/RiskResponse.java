package com.swedbank.web.response;

import com.swedbank.persistence.enums.RiskType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RiskResponse {

    private RiskType type;
    private List<RiskResponseByType> data;

}
