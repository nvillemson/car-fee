package com.swedbank.web.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCarRequest {

    @NotNull
    private Long id;

    @NotNull
    @Size(min = 6, max = 6)
    private String plateNumber;

    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate registration;

    @NotNull
    @Min(250)
    private Long purchasePrice;

    @NotNull
    @Size(min = 1, max = 25)
    private String producer;

    @NotNull
    @Min(0)
    private Long mileage;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer=8, fraction=2)
    private BigDecimal previousIndemnity;

    @NotNull
    private Boolean previousIndemnityUsed;

}
