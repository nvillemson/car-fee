package com.swedbank.web.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GetCarsRequest {

    @Min(0)
    private Integer offset;

    @Min(0)
    private Integer limit;

}
