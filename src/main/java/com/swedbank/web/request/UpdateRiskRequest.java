package com.swedbank.web.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateRiskRequest {

    @NotNull
    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer=8, fraction=2)
    private BigDecimal value;
}
