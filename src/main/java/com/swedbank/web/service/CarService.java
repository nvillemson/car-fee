package com.swedbank.web.service;

import com.swedbank.persistence.model.Car;
import com.swedbank.persistence.model.Risk;
import com.swedbank.persistence.repository.CarCrudRepository;
import com.swedbank.persistence.repository.RiskCrudRepository;
import com.swedbank.web.exceptions.AveragePriceNotListedException;
import com.swedbank.web.exceptions.CarNotFoundException;
import com.swedbank.web.exceptions.UnreadableFileException;
import com.swedbank.web.response.CarResponse;
import com.swedbank.web.response.IdResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class CarService {

    private final RiskCrudRepository riskCrudRepository;
    private final CarCrudRepository carCrudRepository;

    public IdResponse addCar(String plateNumber, LocalDate registration, Long purchasePrice, String producer, Long mileage,
                             BigDecimal previousIndemnity, Boolean previousIndemnityUsed) {
        BigDecimal cascoAnnual = calculatePrice(registration, purchasePrice, producer, mileage, previousIndemnity, previousIndemnityUsed);
        if (cascoAnnual == null) {
            throw new AveragePriceNotListedException("Car with plateNumber " + plateNumber + " and producer "
                    + producer + " was not saved.");
        }
        Car saved = carCrudRepository.save(new Car(null, plateNumber, registration, purchasePrice, producer, mileage,
                previousIndemnity, cascoAnnual, previousIndemnityUsed, null));
        return new IdResponse(saved.getId());

    }

    public void updateCar(Long id, String plateNumber, LocalDate registration, Long purchasePrice, String producer, Long mileage,
                          BigDecimal previousIndemnity, Boolean previousIndemnityUsed) {
        Optional<Car> car = carCrudRepository.findById(id);
        if (car.isEmpty()) {
            throw new CarNotFoundException("No car with id " + id +".");
        }

        BigDecimal cascoAnnual = calculatePrice(registration, purchasePrice, producer, mileage, previousIndemnity, previousIndemnityUsed);
        if (cascoAnnual == null) {
            throw new AveragePriceNotListedException("Car with plateNumber " + plateNumber + " and producer "
                    + producer + " was not saved.");
        }
        carCrudRepository.updateCar(registration, purchasePrice, producer, mileage, previousIndemnity, cascoAnnual, previousIndemnityUsed, id);
    }

    public void deleteCar(Long id) {
        carCrudRepository.removeCar(id);
    }

    public List<CarResponse> getCars(Integer offset, Integer limit) {
        Iterable<Car> cars = carCrudRepository.listCars(offset, limit);

        List<CarResponse> output = new LinkedList<>();
        cars.forEach(car -> {
            output.add(new CarResponse(car.getId(), car.getPlateNumber(), car.getRegistration(), car.getPurchasePrice(), car.getProducer(),
                    car.getMileage(), car.getPreviousIndemnity(), car.getCascoAnnual(), car.getPreviousIndemnityUsed(), car.getDeleted()));
        });

        return output;
    }

    // start with 1, as template has first field unnecessary
    // no casco - then recalculate no casco afterwards
    public void parseAndSave(MultipartFile file) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineParts = line.split(",");
                LocalDate registration = LocalDate.now().withYear(Integer.parseInt(lineParts[2]));
                BigDecimal cascoAnnual = calculatePrice(registration, Long.parseLong(lineParts[3]), lineParts[4],
                        Long.parseLong(lineParts[5]), BigDecimal.valueOf(Double.parseDouble(lineParts[6])), false);
                carCrudRepository.save(new Car(null, lineParts[1], registration, Long.parseLong(lineParts[3]), lineParts[4],
                        Long.parseLong(lineParts[5]), cascoAnnual, BigDecimal.valueOf(Double.parseDouble(lineParts[6])), false, null));
            }
        } catch (Exception ex) {
            throw new UnreadableFileException(ex.getMessage());
        }
    }

    private BigDecimal calculatePrice(LocalDate registration, Long purchasePrice, String producer, Long mileage,
                                     BigDecimal previousIndemnity, Boolean previousIndemnityUsed) {
        Iterable<Risk> nonDeletedRisks = riskCrudRepository.getNonDeletedRisks();
        List<Risk> purchasePriceRisks = new LinkedList<>();
        List<Risk> makeCoefficientsRisks = new LinkedList<>();
        List<Risk> coefficientsRisks = new LinkedList<>();
        nonDeletedRisks.forEach(risk -> {
            switch (risk.getType()) {
                case AVG_PURCHASE_PRICE -> purchasePriceRisks.add(risk);
                case MAKE_COEFFICIENTS -> makeCoefficientsRisks.add(risk);
                case COEFFICIENTS -> coefficientsRisks.add(risk);
            }
        });

        Risk purchasePriceRiskFound = purchasePriceRisks.stream()
                .filter(risk -> producer.equalsIgnoreCase(risk.getName()))
                .findAny()
                .orElse(null);

        if (purchasePriceRiskFound == null) {
            return null;
        }

        Risk makeCoefficientsFound = makeCoefficientsRisks.stream()
                .filter(risk -> producer.equalsIgnoreCase(risk.getName()))
                .findAny()
                .orElse(null);

        BigDecimal makeCoefficientsValue = makeCoefficientsFound != null ? makeCoefficientsFound.getValue() : BigDecimal.ONE;

        BigDecimal earlyCasco = BigDecimal.ZERO;
        BigDecimal age = BigDecimal.valueOf(LocalDate.now().getYear()).subtract(BigDecimal.valueOf(registration.getYear()));
        for (Risk risk : coefficientsRisks) {
            switch (risk.getName()) {
                case "vehicle_age" -> earlyCasco = earlyCasco.add(risk.getValue().multiply(age));
                case "vehicle_mile_age" -> earlyCasco = earlyCasco.add(risk.getValue().multiply(BigDecimal.valueOf(mileage)));
                case "vehicle_value" -> earlyCasco = earlyCasco.add(risk.getValue().multiply(BigDecimal.valueOf(purchasePrice)));
                case "previous_indemnity" -> {
                    if (previousIndemnityUsed != null && previousIndemnityUsed) {
                        earlyCasco = earlyCasco.add(risk.getValue().multiply(previousIndemnity));
                    }
                }
            }
        }

        return earlyCasco.multiply(makeCoefficientsValue);
    }

}
