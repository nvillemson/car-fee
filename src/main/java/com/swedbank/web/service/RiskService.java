package com.swedbank.web.service;

import com.swedbank.persistence.enums.RiskType;
import com.swedbank.persistence.model.Risk;
import com.swedbank.persistence.repository.RiskCrudRepository;
import com.swedbank.web.response.IdResponse;
import com.swedbank.web.response.RiskResponse;
import com.swedbank.web.response.RiskResponseByType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class RiskService {

    private final RiskCrudRepository riskCrudRepository;

    public IdResponse addRisk(String riskName, RiskType riskType, BigDecimal riskValue) {
        Risk saved = riskCrudRepository.save(new Risk(null, riskType, riskName, riskValue, null));
        return new IdResponse(saved.getId());
    }

    public List<RiskResponse> getAllRisks() {
        Iterable<Risk> all = riskCrudRepository.getNonDeletedRisks();

        Map<RiskType, List<Risk>> risksByType = new HashMap<>();
        all.forEach(risk -> {
            List<Risk> risks = risksByType.get(risk.getType());
            if (risks == null) {
                risks = new LinkedList<>();
            }
            risks.add(risk);
            risksByType.put(risk.getType(), risks);
        });

        List<RiskResponse> output = new LinkedList<>();
        risksByType.forEach((key, value) -> {
            List<RiskResponseByType> data = new LinkedList<>();
            value.forEach(actualRisk ->
                    data.add(new RiskResponseByType(actualRisk.getId(), actualRisk.getName(),
                            actualRisk.getValue()))
                    );
            output.add(new RiskResponse(key, data));
        });

        return output;
    }

    public IdResponse updateRisk(Long id, String name, BigDecimal value) {
        riskCrudRepository.updateRisk(id, name, value);
        return new IdResponse(id);
    }

    public void deleteRisk(Long id) {
        riskCrudRepository.removeRisk(id);
    }
}
