package com.swedbank.web.handler;

import com.swedbank.web.exceptions.AveragePriceNotListedException;
import com.swedbank.web.exceptions.CarNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings("unused")
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AveragePriceNotListedException.class)
    protected ResponseEntity<String> handleCarNotSaved(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(CarNotFoundException.class)
    protected ResponseEntity<String> handleCarNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }
}
